// TODO: Better error handling

export const postKey = async (key) => {
  const res = await fetch("/api/key", {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify({ apiKey: key }),
  })

  if (!res.ok) {
    throw new Error(`API request failed with status ${res.status}`)
  }

  return true
}

export const getKey = async (key) => {
  const res = await fetch(`/api/key?apiKey=${key}`)

  if (!res.ok) {
    throw new Error(`API request failed with status ${res.status}`)
  }

  return res.json()
}

export const increaseKey = async (key) => {
  const res = await fetch(`/api/key?apiKey=${key}`, {
    method: "PUT",
    headers: {
      "Content-Type": "application/json",
    },
  })

  if (!res.ok) {
    throw new Error(`API request failed with status ${res.status}`)
  }

  return res
}

export const deleteKey = async (key) => {
  const res = await fetch(`/api/key?apiKey=${key}`, {
    method: "DELETE",
    headers: {
      "Content-Type": "application/json",
    },
  })

  if (!res.ok) {
    throw new Error(`API request failed with status ${res.status}`)
  }

  return res.json()
}
