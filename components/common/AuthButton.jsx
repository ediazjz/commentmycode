"use client"

import { signIn, signOut } from "next-auth/react"

import { useSession } from "next-auth/react"

import { postKey, getKey, increaseKey, deleteKey } from "@/lib/queries"

export const AuthButton = () => {
  const { data: session, status } = useSession()

  if (status === "authenticated") {
    return (
      <>
        <p>Data {session.user.name}:</p>
        <br />
        <button onClick={() => postKey("test")}>Save to database </button>
        <button onClick={() => getKey("test")}>Read from database</button>
        <button onClick={() => increaseKey("test")}>Increase</button>
        <button onClick={() => deleteKey("test")}>Delete</button>
        <button onClick={() => signOut()}>Sign out</button>
      </>
    )
  }

  return (
    <>
      Not signed in <br />
      <button onClick={() => signIn()}>Sign in</button>
    </>
  )
}
