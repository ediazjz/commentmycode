// import { useState } from "react"

import { AuthButton } from "@/components"
import styles from "./index.module.css"
import "@/styles/globals.css"

export const metadata = {
  title: "My Page Title",
}

export default function Page() {
  // const [animalInput, setAnimalInput] = useState("")
  // const [result, setResult] = useState()

  // async function onSubmit(event) {
  //   event.preventDefault()

  //   try {
  //     const response = await fetch("/api/gpt/comment", {
  //       method: "POST",
  //       headers: {
  //         "Content-Type": "application/json",
  //       },
  //       body: JSON.stringify({ animal: animalInput }),
  //     })

  //     const data = await response.json()
  //     if (response.status !== 200) {
  //       throw (
  //         data.error ||
  //         new Error(`Request failed with status ${response.status}`)
  //       )
  //     }

  //     setResult(data.result)
  //     setAnimalInput("")
  //   } catch (error) {
  //     // Consider implementing your own error handling logic here
  //     console.error("here", error)
  //     alert(error.message)
  //   }
  // }

  return (
    <main className={styles.main}>
      <AuthButton />

      <h3>Name my pet</h3>
      {/* <form onSubmit={onSubmit}>
        <input
          type="text"
          name="animal"
          placeholder="Enter an animal"
          value={animalInput}
          onChange={(e) => setAnimalInput(e.target.value)}
        />
        <input type="submit" value="Generate names" />
      </form> */}
      {/* <div className={styles.result}>{result}</div> */}
    </main>
  )
}
