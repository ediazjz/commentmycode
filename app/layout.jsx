// Why do I need to add .jsx for it to work?
// When it was .js I had no such problem
import Provider from "./provider.jsx"

export default function RootLayout({ children }) {
  return (
    <html lang="en">
      <body>
        <Provider>{children}</Provider>
      </body>
    </html>
  )
}
