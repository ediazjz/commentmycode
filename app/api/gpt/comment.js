import { generateSuperHeroAnimals, openai } from "@/lib/gpt"

export default async function createComment(req, res) {
  const animal = req.body.animal || ""
  if (animal.trim().length === 0) {
    res.status(400).json({
      error: {
        message: "Please enter a valid animal",
      },
    })
    return
  }

  try {
    const completion = await openai.createCompletion({
      model: "text-davinci-003",
      prompt: generateSuperHeroAnimals(animal),
      temperature: 0.6,
    })

    res.status(200).json({ result: completion.data.choices[0].text })
  } catch (error) {
    // Consider adjusting the error handling logic for your use case
    if (error.response) {
      console.error(error.response.status, error.response.data)

      res.status(error.response.status).json(error.response.data)
    } else {
      console.error(`Error with OpenAI API request: ${error.message}`)

      res.status(500).json({
        error: {
          message: "An error occurred during your request.",
        },
      })
    }
  }
}
