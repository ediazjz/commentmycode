import { NextResponse } from "next/server"

import { kv } from "@vercel/kv"

export async function POST(req) {
  const body = await req.json()

  const res = await kv.set(body.apiKey, 0)

  return NextResponse.json({ data: true })
}

export async function GET(req) {
  const apiKey = new URL(req.url).searchParams.get("apiKey")

  const res = await kv.get(apiKey)

  return NextResponse.json(res)
}

export async function PUT(req) {
  const apiKey = new URL(req.url).searchParams.get("apiKey")

  const res = await kv.incrby(apiKey, 1)

  return NextResponse.json(res)
}

export async function DELETE(req) {
  const apiKey = new URL(req.url).searchParams.get("apiKey")

  const res = await kv.del(apiKey)

  return NextResponse.json(res)
}
